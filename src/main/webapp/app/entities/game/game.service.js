(function() {
    'use strict';
    angular
        .module('gamesApp')
        .factory('Game', Game);

    Game.$inject = ['$resource', 'DateUtils'];

    function Game ($resource, DateUtils) {
        var resourceUrl =  'api/games/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.registrationDate = DateUtils.convertLocalDateFromServer(data.registrationDate);
                        data.sellDate = DateUtils.convertLocalDateFromServer(data.sellDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.registrationDate = DateUtils.convertLocalDateToServer(data.registrationDate);
                    data.sellDate = DateUtils.convertLocalDateToServer(data.sellDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.registrationDate = DateUtils.convertLocalDateToServer(data.registrationDate);
                    data.sellDate = DateUtils.convertLocalDateToServer(data.sellDate);
                    return angular.toJson(data);
                }
            },
            'getGamesByGenre': {
                url: 'api/games/getGamesByGenre',
                method: 'GET',
                isArray: true
            },
            'getRateByYear': {
                url: 'api/games/getRateByYear',
                method: 'GET',
                isArray: true
            }
        });
    }
})();
