(function () {
    'use strict';

    angular
        .module('gamesApp')
        .controller('GameDialogController', GameDialogController);

    GameDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Game', 'GameSearch'];

    function GameDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Game, GameSearch) {
        var vm = this;

        vm.game = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.game.id !== null) {
                Game.update(vm.game, onSaveSuccess, onSaveError);
            } else {
                Game.save(vm.game, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('gamesApp:gameUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        $scope.getLocation = function (val,mode) {
            return GameSearch.query({
                query:  mode ==='genre'?  'genre:'+val+'*' : 'platform:'+val+'*',
                page: 0,
                size: 8,
                sort: 'asc'})
                .$promise
                .then(function (response) {
                return response.map(function (item) {
                    return  mode ==='genre' ?  item.genre : item.platform;
                });
            });
        };

        // Game.query({
        //     page: pagingParams.page - 1,
        //     size: vm.itemsPerPage,
        //     sort: sort()
        // }, onSuccess, onError);
        //
        //
        // function onSuccess(data, headers) {
        //     vm.links = ParseLinks.parse(headers('link'));
        //     vm.totalItems = headers('X-Total-Count');
        //     vm.queryCount = vm.totalItems;
        //     vm.games = data;
        //     vm.page = pagingParams.page;
        // }

        vm.datePickerOpenStatus.registrationDate = false;
        vm.datePickerOpenStatus.sellDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
