(function () {
        'use strict';

        angular
            .module('gamesApp')
            .controller('GameController', GameController);

        GameController.$inject = ['$scope', '$state', 'Game', 'GameSearch', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants'];

        function GameController($scope, $state, Game, GameSearch, ParseLinks, AlertService, pagingParams, paginationConstants) {
            var vm = this;

            vm.loadPage = loadPage;
            vm.predicate = pagingParams.predicate;
            vm.reverse = pagingParams.ascending;
            vm.transition = transition;
            vm.itemsPerPage = paginationConstants.itemsPerPage;
            vm.clear = clear;
            vm.search = search;
            vm.loadAll = loadAll;
            vm.searchQuery = pagingParams.search;
            vm.currentSearch = pagingParams.search;


            loadGenreCharts();
            loadRateByYearCharts();

            loadAll();


            function loadRateByYearCharts() {
                Game.getRateByYear({}, function (data) {

                    var cat = [];
                    data[0].data.map(function (item) {
                        cat.push(item.name);
                    });

                    $('#value_chart').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Valor estimado por ano de lançamento '
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Valor'
                            },
                            stackLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                }
                            }
                        },
                        legend: {
                            align: 'right',
                            x: -30,
                            verticalAlign: 'top',
                            y: 25,
                            floating: true,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: false
                        },
                        xAxis: {
                            categories: cat
                        },

                        plotOptions: {
                            column: {
                                stacking: 'normal'
                            }
                        },
                        series: data
                    });
                });
            }

            function loadGenreCharts() {

                Game.getGamesByGenre({}, function (data) {

                    $('#genre_chart').highcharts({
                        chart: {
                            type: 'pie'
                        },
                        title: {
                            text: 'Gênero por genero'
                        },
                        legend: {
                            align: 'right',
                            x: -30,
                            verticalAlign: 'top',
                            y: 25,
                            floating: true,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: false
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true,
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [
                            {
                                name: 'Gênero',
                                data: data
                            }]
                    });
                });
            }

            function loadAll() {
                if (pagingParams.search) {
                    GameSearch.query({
                        query: pagingParams.search,
                        page: pagingParams.page - 1,
                        size: vm.itemsPerPage,
                        sort: sort()
                    }, onSuccess, onError);
                } else {
                    Game.query({
                        page: pagingParams.page - 1,
                        size: vm.itemsPerPage,
                        sort: sort()
                    }, onSuccess, onError);
                }
                function sort() {
                    var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                    if (vm.predicate !== 'id') {
                        result.push('id');
                    }
                    return result;
                }

                function onSuccess(data, headers) {
                    vm.links = ParseLinks.parse(headers('link'));
                    vm.totalItems = headers('X-Total-Count');
                    vm.queryCount = vm.totalItems;
                    vm.games = data;
                    vm.page = pagingParams.page;
                }

                function onError(error) {
                    AlertService.error(error.data.message);
                }
            }

            function loadPage(page) {
                vm.page = page;
                vm.transition();
            }

            function transition() {
                $state.transitionTo($state.$current, {
                    page: vm.page,
                    sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                    search: vm.currentSearch
                });
            }

            function search(searchQuery) {
                if (!searchQuery) {
                    return vm.clear();
                }
                vm.links = null;
                vm.page = 1;
                vm.predicate = '_score';
                vm.reverse = false;
                vm.currentSearch = searchQuery;
                vm.transition();
            }

            function clear() {
                vm.links = null;
                vm.page = 1;
                vm.predicate = 'id';
                vm.reverse = true;
                vm.currentSearch = null;
                vm.transition();
            }
        }
    })();
