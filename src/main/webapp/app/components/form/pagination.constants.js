(function() {
    'use strict';

    angular
        .module('gamesApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
