package br.com.leandrojacomelli.service.dto;

import java.math.BigDecimal;

/**
 * Created by leandro on 9/18/16.
 */
public class ChartDataDTO {

    String name;
    Double y;

    public ChartDataDTO(String name, Double y) {
        this.name = name;
        this.y = y;
    }

    public ChartDataDTO(String name, Long y) {
        this.name = name;
        this.y = y.doubleValue();
    }

    public ChartDataDTO(Integer  name, BigDecimal y) {
        this.name = name.toString();
        this.y = y.doubleValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }
}
