package br.com.leandrojacomelli.service.dto;

import java.util.List;

/**
 * Created by leandro on 9/18/16.
 */
public class ChartSeriesDTO {

    String name;
    List<ChartDataDTO> data;

    public ChartSeriesDTO(String name, List<ChartDataDTO> data) {
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChartDataDTO> getData() {
        return data;
    }

    public void setData(List<ChartDataDTO> data) {
        this.data = data;
    }
}
