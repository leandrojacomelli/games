package br.com.leandrojacomelli.service.impl;

import br.com.leandrojacomelli.domain.Game;
import br.com.leandrojacomelli.repository.GameRepository;
import br.com.leandrojacomelli.repository.search.GameSearchRepository;
import br.com.leandrojacomelli.service.GameService;
import br.com.leandrojacomelli.service.dto.ChartDataDTO;
import br.com.leandrojacomelli.service.dto.ChartSeriesDTO;
import br.com.leandrojacomelli.service.dto.GameDTO;
import br.com.leandrojacomelli.service.mapper.GameMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDate.now;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Game.
 */
@Service
@Transactional
public class GameServiceImpl implements GameService {

    private final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    @Inject
    private GameRepository gameRepository;

    @Inject
    private GameMapper gameMapper;

    @Inject
    private GameSearchRepository gameSearchRepository;

    @Inject
    private EntityManager em;


    /**
     * Save a game.
     *
     * @param gameDTO the entity to save
     * @return the persisted entity
     */
    public GameDTO save(GameDTO gameDTO) {
        log.debug("Request to save Game : {}", gameDTO);
        Game game = gameMapper.gameDTOToGame(gameDTO);
        game.setRegistrationDate(now());
        game = gameRepository.save(game);
        GameDTO result = gameMapper.gameToGameDTO(game);
        gameSearchRepository.save(game);
        return result;
    }

    /**
     * Get all the games.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GameDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Games");
        Page<Game> result = gameRepository.findAll(pageable);
        return result.map(game -> gameMapper.gameToGameDTO(game));
    }

    /**
     * Get one game by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GameDTO findOne(Long id) {
        log.debug("Request to get Game : {}", id);
        Game game = gameRepository.findOne(id);
        GameDTO gameDTO = gameMapper.gameToGameDTO(game);
        return gameDTO;
    }

    /**
     * Delete the  game by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Game : {}", id);
        gameRepository.delete(id);
        gameSearchRepository.delete(id);
    }

    /**
     * Search for the game corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GameDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Games for query {}", query);
        Page<Game> result = gameSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(game -> gameMapper.gameToGameDTO(game));
    }

    @Override
    public List<ChartDataDTO> getGamesByGenre() {
        List<ChartDataDTO> resultList = em.createNamedQuery(Game.countGamesByGenre, ChartDataDTO.class).getResultList();
        return resultList;
    }

    @Override
    public List<ChartSeriesDTO> getRateByYear() {
        List<ChartDataDTO> resultList = em.createNamedQuery(Game.sumEstimatedByRelease, ChartDataDTO.class).getResultList();

        Double sum = resultList.stream().mapToDouble(e -> e.getY()).sum();
        List<ChartSeriesDTO> result = new ArrayList<>();
        result.add(new ChartSeriesDTO(sum.toString(), resultList));


        return result;
    }


}
