package br.com.leandrojacomelli.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Game.
 */
@Entity
@Table(name = "game")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "game")
@NamedQueries({
    @NamedQuery(name = Game.countGamesByGenre,
        query = "select  new br.com.leandrojacomelli.service.dto.ChartDataDTO (g.genre , count (1)) from Game g group by g.genre"
    ),
    @NamedQuery(name = Game.sumEstimatedByRelease,
        query = "select  new br.com.leandrojacomelli.service.dto.ChartDataDTO (g.releaseYear, sum(g.estimatedValue)) from Game g group by g.releaseYear order by g.releaseYear"
    )
})


public class Game implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String countGamesByGenre = "Game.countGamesByGenre";
    public static final String sumEstimatedByRelease = "Game.sumEstimatedByRelease";


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "estimated_value", precision = 10, scale = 2)
    private BigDecimal estimatedValue;

    @NotNull
    @Column(name = "release_year", nullable = false)
    private Integer releaseYear;

    @Min(value = 0)
    @Max(value = 5)
    @Column(name = "rate")
    private Integer rate;

    @NotNull
    @Column(name = "registration_date", nullable = false)
    private LocalDate registrationDate;

    @Column(name = "sell_date")
    private LocalDate sellDate;

    @NotNull
    @Column(name = "genre", nullable = false)
    private String genre;

    @NotNull
    @Column(name = "platform", nullable = false)
    private String platform;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Game title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getEstimatedValue() {
        return estimatedValue;
    }

    public Game estimatedValue(BigDecimal estimatedValue) {
        this.estimatedValue = estimatedValue;
        return this;
    }

    public void setEstimatedValue(BigDecimal estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public Game releaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
        return this;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Integer getRate() {
        return rate;
    }

    public Game rate(Integer rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public Game registrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public LocalDate getSellDate() {
        return sellDate;
    }

    public Game sellDate(LocalDate sellDate) {
        this.sellDate = sellDate;
        return this;
    }

    public void setSellDate(LocalDate sellDate) {
        this.sellDate = sellDate;
    }

    public String getGenre() {
        return genre;
    }

    public Game genre(String genre) {
        this.genre = genre;
        return this;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPlatform() {
        return platform;
    }

    public Game platform(String platform) {
        this.platform = platform;
        return this;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Game game = (Game) o;
        if (game.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, game.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Game{" +
            "id=" + id +
            ", title='" + title + "'" +
            ", estimatedValue='" + estimatedValue + "'" +
            ", releaseYear='" + releaseYear + "'" +
            ", rate='" + rate + "'" +
            ", registrationDate='" + registrationDate + "'" +
            ", sellDate='" + sellDate + "'" +
            '}';
    }
}
