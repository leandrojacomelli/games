package br.com.leandrojacomelli.web.rest;

import br.com.leandrojacomelli.GamesApp;

import br.com.leandrojacomelli.domain.Game;
import br.com.leandrojacomelli.repository.GameRepository;
import br.com.leandrojacomelli.service.GameService;
import br.com.leandrojacomelli.repository.search.GameSearchRepository;
import br.com.leandrojacomelli.service.dto.GameDTO;
import br.com.leandrojacomelli.service.mapper.GameMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GameResource REST controller.
 *
 * @see GameResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamesApp.class)
public class GameResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";

    private static final String DEFAULT_PLATFORM = "AAAAA";
    private static final String UPDATED_PLATFORM = "BBBBB";

    private static final String DEFAULT_GENRE = "AAAAA";
    private static final String UPDATED_GENRE = "BBBBB";

    private static final BigDecimal DEFAULT_ESTIMATED_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_ESTIMATED_VALUE = new BigDecimal(2);

    private static final Integer DEFAULT_RELEASE_YEAR = 1;
    private static final Integer UPDATED_RELEASE_YEAR = 2;

    private static final Integer DEFAULT_RATE = 0;
    private static final Integer UPDATED_RATE = 1;

    private static final LocalDate DEFAULT_REGISTRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REGISTRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_SELL_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SELL_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private GameRepository gameRepository;

    @Inject
    private GameMapper gameMapper;

    @Inject
    private GameService gameService;

    @Inject
    private GameSearchRepository gameSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restGameMockMvc;

    private Game game;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GameResource gameResource = new GameResource();
        ReflectionTestUtils.setField(gameResource, "gameService", gameService);
        this.restGameMockMvc = MockMvcBuilders.standaloneSetup(gameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Game createEntity(EntityManager em) {
        Game game = new Game()
                .title(DEFAULT_TITLE)
                .estimatedValue(DEFAULT_ESTIMATED_VALUE)
                .releaseYear(DEFAULT_RELEASE_YEAR)
                .rate(DEFAULT_RATE)
                .registrationDate(DEFAULT_REGISTRATION_DATE)
                .sellDate(DEFAULT_SELL_DATE)
                .genre(DEFAULT_GENRE)
                .platform(DEFAULT_PLATFORM);

        return game;
    }

    @Before
    public void initTest() {
        gameSearchRepository.deleteAll();
        game = createEntity(em);
    }

    @Test
    @Transactional
    public void createGame() throws Exception {
        int databaseSizeBeforeCreate = gameRepository.findAll().size();

        // Create the Game
        GameDTO gameDTO = gameMapper.gameToGameDTO(game);

        restGameMockMvc.perform(post("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gameDTO)))
                .andExpect(status().isCreated());

        // Validate the Game in the database
        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeCreate + 1);
        Game testGame = games.get(games.size() - 1);
        assertThat(testGame.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testGame.getEstimatedValue()).isEqualTo(DEFAULT_ESTIMATED_VALUE);
        assertThat(testGame.getReleaseYear()).isEqualTo(DEFAULT_RELEASE_YEAR);
        assertThat(testGame.getRate()).isEqualTo(DEFAULT_RATE);

        assertThat(testGame.getSellDate()).isEqualTo(DEFAULT_SELL_DATE);

        // Validate the Game in ElasticSearch
        Game gameEs = gameSearchRepository.findOne(testGame.getId());
        assertThat(gameEs).isEqualToComparingFieldByField(testGame);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = gameRepository.findAll().size();
        // set the field null
        game.setTitle(null);

        // Create the Game, which fails.
        GameDTO gameDTO = gameMapper.gameToGameDTO(game);

        restGameMockMvc.perform(post("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gameDTO)))
                .andExpect(status().isBadRequest());

        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReleaseYearIsRequired() throws Exception {
        int databaseSizeBeforeTest = gameRepository.findAll().size();
        // set the field null
        game.setReleaseYear(null);

        // Create the Game, which fails.
        GameDTO gameDTO = gameMapper.gameToGameDTO(game);

        restGameMockMvc.perform(post("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gameDTO)))
                .andExpect(status().isBadRequest());

        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void getAllGames() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);

        // Get all the games
        restGameMockMvc.perform(get("/api/games?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(game.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].estimatedValue").value(hasItem(DEFAULT_ESTIMATED_VALUE.intValue())))
                .andExpect(jsonPath("$.[*].releaseYear").value(hasItem(DEFAULT_RELEASE_YEAR)))
                .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE)))
                .andExpect(jsonPath("$.[*].registrationDate").value(hasItem(DEFAULT_REGISTRATION_DATE.toString())))
                .andExpect(jsonPath("$.[*].sellDate").value(hasItem(DEFAULT_SELL_DATE.toString())));
    }

    @Test
    @Transactional
    public void getGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);

        // Get the game
        restGameMockMvc.perform(get("/api/games/{id}", game.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(game.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.estimatedValue").value(DEFAULT_ESTIMATED_VALUE.intValue()))
            .andExpect(jsonPath("$.releaseYear").value(DEFAULT_RELEASE_YEAR))
            .andExpect(jsonPath("$.rate").value(DEFAULT_RATE))
            .andExpect(jsonPath("$.registrationDate").value(DEFAULT_REGISTRATION_DATE.toString()))
            .andExpect(jsonPath("$.sellDate").value(DEFAULT_SELL_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGame() throws Exception {
        // Get the game
        restGameMockMvc.perform(get("/api/games/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);
        gameSearchRepository.save(game);
        int databaseSizeBeforeUpdate = gameRepository.findAll().size();

        // Update the game
        Game updatedGame = gameRepository.findOne(game.getId());
        updatedGame
                .title(UPDATED_TITLE)
                .estimatedValue(UPDATED_ESTIMATED_VALUE)
                .releaseYear(UPDATED_RELEASE_YEAR)
                .rate(UPDATED_RATE)
                .registrationDate(UPDATED_REGISTRATION_DATE)
                .sellDate(UPDATED_SELL_DATE);
        GameDTO gameDTO = gameMapper.gameToGameDTO(updatedGame);

        restGameMockMvc.perform(put("/api/games")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gameDTO)))
                .andExpect(status().isOk());

        // Validate the Game in the database
        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeUpdate);
        Game testGame = games.get(games.size() - 1);
        assertThat(testGame.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testGame.getEstimatedValue()).isEqualTo(UPDATED_ESTIMATED_VALUE);
        assertThat(testGame.getReleaseYear()).isEqualTo(UPDATED_RELEASE_YEAR);
        assertThat(testGame.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testGame.getRegistrationDate()).isEqualTo(UPDATED_REGISTRATION_DATE);
        assertThat(testGame.getSellDate()).isEqualTo(UPDATED_SELL_DATE);

        // Validate the Game in ElasticSearch
        Game gameEs = gameSearchRepository.findOne(testGame.getId());
        assertThat(gameEs).isEqualToComparingFieldByField(testGame);
    }

    @Test
    @Transactional
    public void deleteGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);
        gameSearchRepository.save(game);
        int databaseSizeBeforeDelete = gameRepository.findAll().size();

        // Get the game
        restGameMockMvc.perform(delete("/api/games/{id}", game.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean gameExistsInEs = gameSearchRepository.exists(game.getId());
        assertThat(gameExistsInEs).isFalse();

        // Validate the database is empty
        List<Game> games = gameRepository.findAll();
        assertThat(games).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGame() throws Exception {
        // Initialize the database
        gameRepository.saveAndFlush(game);
        gameSearchRepository.save(game);

        // Search the game
        restGameMockMvc.perform(get("/api/_search/games?query=id:" + game.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(game.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].estimatedValue").value(hasItem(DEFAULT_ESTIMATED_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].releaseYear").value(hasItem(DEFAULT_RELEASE_YEAR)))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE)))
            .andExpect(jsonPath("$.[*].registrationDate").value(hasItem(DEFAULT_REGISTRATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].sellDate").value(hasItem(DEFAULT_SELL_DATE.toString())));
    }
}
